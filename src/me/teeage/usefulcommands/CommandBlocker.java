package me.teeage.usefulcommands;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

public class CommandBlocker implements Listener {

	@EventHandler(priority = EventPriority.MONITOR)
	public void CommandPreprocess(PlayerCommandPreprocessEvent ev) {
		if ((ev.getMessage().toLowerCase().startsWith("/plugins")) || (ev.getMessage().toLowerCase().startsWith("/pl"))
				|| (ev.getMessage().toLowerCase().startsWith("/?"))
				|| (ev.getMessage().toLowerCase().startsWith("/help"))
				|| (ev.getMessage().toLowerCase().startsWith("/ver"))
				|| (ev.getMessage().toLowerCase().startsWith("/version"))
				|| (ev.getMessage().toLowerCase().startsWith("/bukkit"))) {
			if (!ev.getPlayer().hasPermission("uc.cmdblocker.bypass")) {
				ev.setCancelled(true);
				ev.setMessage(CommandsMain.getInstance().noperm);
				ev.getPlayer().sendMessage(CommandsMain.getInstance().noperm);
			}
		}
	}

}
