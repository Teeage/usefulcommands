package me.teeage.usefulcommands;

import java.awt.List;
import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.WorldCreator;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import me.teeage.usefulcommands.command.CommandHandler;
import me.teeage.usefulcommands.command.VanishCommand;
import me.teeage.usefulcommands.util.Metrics;

public class CommandsMain extends JavaPlugin {

	private static CommandsMain instance;
	public String prefix;
	public String noperm;
	private boolean pluginsBlocker;
	public String kickReason;
	public YamlConfiguration cfg = YamlConfiguration.loadConfiguration(new File(getDataFolder() + "//worlds.yml"));

	@Override
	public void onEnable() {
		instance = this;
		check("Prefix", "&7[&5UC&7] ");
		check("noPermissions", "&cNo Permissions!");
		check("PluginsBlocker", true);
		registerCommands();
		CommandHandler.load();
		try {
			Metrics metrics = new Metrics(this);
			metrics.start();
		} catch (IOException e) {
			e.printStackTrace();
		}
		prefix = ChatColor.translateAlternateColorCodes('&', getConfig().getString("Prefix"));
		noperm = (this.prefix + ChatColor.translateAlternateColorCodes('&', getConfig().getString("noPermissions")));
		pluginsBlocker = getConfig().getBoolean("PluginsBlocker");
		registerEvents();
		if (!cfg.contains("worlds")) {
			cfg.set("worlds", new List());
			try {
				cfg.save(new File(getDataFolder() + "worlds.yml"));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (cfg.getStringList("worlds") != null || !cfg.getStringList("worlds").isEmpty()) {
			for (String s : cfg.getStringList("worlds")) {
				Bukkit.createWorld(new WorldCreator(s));
			}
		}

	}

	private void registerEvents() {
		if (pluginsBlocker) {
			Bukkit.getPluginManager().registerEvents(new CommandBlocker(), this);
		}
		Bukkit.getPluginManager().registerEvents(new VanishCommand(), this);
	}

	private void registerCommands() {
		getCommand("uc").setExecutor(new CommandHandler());
		getCommand("heal").setExecutor(new CommandHandler());
		getCommand("feed").setExecutor(new CommandHandler());
		getCommand("gm").setExecutor(new CommandHandler());
		getCommand("ping").setExecutor(new CommandHandler());
		getCommand("clearchat").setExecutor(new CommandHandler());
		getCommand("fly").setExecutor(new CommandHandler());
		getCommand("nick").setExecutor(new CommandHandler());
		getCommand("vanish").setExecutor(new CommandHandler());
		getCommand("day").setExecutor(new CommandHandler());
		getCommand("night").setExecutor(new CommandHandler());
		getCommand("kick").setExecutor(new CommandHandler());
		getCommand("g").setExecutor(new CommandHandler());
		getCommand("fix").setExecutor(new CommandHandler());
		getCommand("whois").setExecutor(new CommandHandler());
		getCommand("broadcast").setExecutor(new CommandHandler());
		getCommand("multiworld").setExecutor(new CommandHandler());
		getCommand("world").setExecutor(new CommandHandler());
		getCommand("invsee").setExecutor(new CommandHandler());
		getCommand("runas").setExecutor(new CommandHandler());
		getCommand("spawn").setExecutor(new CommandHandler());
		getCommand("ban").setExecutor(new CommandHandler());
		getCommand("unban").setExecutor(new CommandHandler());

	}

	private void check(String path, Object value) {
		if (!getConfig().contains(path)) {
			getConfig().set(path, value);
			saveConfig();
		}
	}

	public static CommandsMain getInstance() {
		return instance;
	}

}
