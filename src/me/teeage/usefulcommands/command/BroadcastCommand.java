package me.teeage.usefulcommands.command;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.entity.Player;

public class BroadcastCommand extends CMD {

	@Override
	public boolean onCommand(Player p, Command cmd, String label, String[] args) {
		if (args.length >= 1) {
			String msg = "";
			for (int i = 0; i < args.length; i++) {
				msg = msg + args[i] + " ";
			}
			Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', msg));
		} else
			p.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.prefix + "§c/broadcast (message...)"));

		return true;
	}

	@Override
	String getPermission() {
		return "uc.broadcast";
	}

}
