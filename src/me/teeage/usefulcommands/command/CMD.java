package me.teeage.usefulcommands.command;

import org.bukkit.command.Command;
import org.bukkit.entity.Player;

import me.teeage.usefulcommands.CommandsMain;

public abstract class CMD {

	protected final CommandsMain plugin = CommandsMain.getInstance();

	abstract boolean onCommand(Player p, Command cmd, String label, String[] args);

	abstract String getPermission();
}
