package me.teeage.usefulcommands.command;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.entity.Player;

public class ClearChatCommand extends CMD {

	@Override
	public boolean onCommand(Player p, Command cmd, String label, String[] args) {
		if (args.length == 0) {
			for (int i = 0; i <= 120; i++) {
				p.sendMessage(" ");
			}
		} else
			p.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.prefix + "§c/clearchat"));
		return true;
	}

	@Override
	String getPermission() {
		return "uc.clearchat";
	}
}
