package me.teeage.usefulcommands.command;

import java.util.HashMap;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.teeage.usefulcommands.CommandsMain;

public class CommandHandler implements CommandExecutor {
	private final CommandsMain plugin = CommandsMain.getInstance();
	public static HashMap<String, CMD> cmds = new HashMap<>();

	public static void load() {
		cmds.put("broadcast", new BroadcastCommand());
		cmds.put("clearchat", new ClearChatCommand());
		cmds.put("day", new DayCommand());
		cmds.put("feed", new FeedCommand());
		cmds.put("fix", new FixCommand());
		cmds.put("fly", new FlyCommand());
		cmds.put("g", new GamemodeCommand());
		cmds.put("gm", new GmCommand());
		cmds.put("heal", new HealCommand());
		cmds.put("invsee", new InvseeCommand());
		cmds.put("kick", new KickCommand());
		cmds.put("multiworld", new MultiworldCommand());
		cmds.put("nick", new NickCommand());
		cmds.put("night", new NightCommand());
		cmds.put("ping", new PingCommand());
		cmds.put("runas", new RunAsCommand());
		cmds.put("spawn", new SpawnCommand());
		cmds.put("uc", new MainCommand());
		cmds.put("vanish", new VanishCommand());
		cmds.put("whois", new WhoIsCommand());
		cmds.put("world", new WorldCommand());
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (sender instanceof Player) {
			Player p = (Player) sender;
			if (cmds.containsKey(cmd.getName())) {
				CMD command = cmds.get(cmd.getName());
				if (p.hasPermission(command.getPermission()) || command.getPermission() == "")
					return command.onCommand(p, cmd, label, args);
				else
					sender.sendMessage(plugin.noperm);
			}
		}
		return true;
	}

}
