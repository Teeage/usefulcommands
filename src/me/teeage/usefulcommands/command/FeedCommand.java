package me.teeage.usefulcommands.command;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.entity.Player;

public class FeedCommand extends CMD {

	@Override
	public boolean onCommand(Player p, Command cmd, String label, String[] args) {
		if (args.length == 0) {
			p.setFoodLevel(20);

		} else if (args.length == 1) {
			Player target = Bukkit.getPlayer(args[0]);
			if (target != null)
				target.setFoodLevel(20);
		} else
			p.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.prefix + "§c/feed [player]"));

		return true;
	}

	@Override
	String getPermission() {
		return "uc.feed";
	}

}
