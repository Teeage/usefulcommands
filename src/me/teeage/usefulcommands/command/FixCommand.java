package me.teeage.usefulcommands.command;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.entity.Player;

public class FixCommand extends CMD {

	@Override
	public boolean onCommand(Player p, Command cmd, String label, String[] args) {
		if (args.length == 0) {
			p.teleport(p.getLocation().add(0, 1.5, 0));
			for (Player player : Bukkit.getOnlinePlayers()) {
				if (p.canSee(player)) {
					p.hidePlayer(player);
					p.showPlayer(player);
				} else {
					p.showPlayer(player);
					p.hidePlayer(player);
				}
			}

		} else
			p.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.prefix + "§c/fix"));
		return true;
	}

	@Override
	String getPermission() {
		return "uc.fix";
	}

}
