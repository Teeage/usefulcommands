package me.teeage.usefulcommands.command;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.entity.Player;

public class FlyCommand extends CMD {

	@Override
	public boolean onCommand(Player p, Command cmd, String label, String[] args) {

		if (args.length == 0) {
			if (p.getAllowFlight()) {
				p.setAllowFlight(false);
				p.setFlying(false);
				p.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.prefix + "Set Fly disabled"));
			} else {
				p.setAllowFlight(true);
				p.setFlying(true);
				p.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.prefix + "Set Fly enabled"));
			}
		}
		if (args.length == 1) {
			if (args[0].equalsIgnoreCase("on")) {
				p.setAllowFlight(true);
				p.setFlying(true);
				p.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.prefix + "Set Fly enabled"));
			}
			if (args[0].equalsIgnoreCase("off")) {
				p.setAllowFlight(false);
				p.setFlying(false);
				p.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.prefix + "Set Fly disabled"));

			}
		} else
			p.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.prefix + "§c/fly"));

		return true;
	}

	@Override
	String getPermission() {
		return "uc.fly";
	}

}
