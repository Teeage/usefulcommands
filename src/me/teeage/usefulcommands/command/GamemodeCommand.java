package me.teeage.usefulcommands.command;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.entity.Player;

public class GamemodeCommand extends CMD {

	@Override
	public boolean onCommand(Player p, Command cmd, String label, String[] args) {
		if (args.length == 0) {
			if (p.getGameMode() == GameMode.SURVIVAL)
				p.setGameMode(GameMode.CREATIVE);
			else if (p.getGameMode() == GameMode.CREATIVE)
				p.setGameMode(GameMode.SURVIVAL);
			else if (p.getGameMode() == GameMode.ADVENTURE)
				p.setGameMode(GameMode.CREATIVE);
			else if (p.getGameMode() == GameMode.SPECTATOR)
				p.setGameMode(GameMode.CREATIVE);
			else if (p.getGameMode() == GameMode.CREATIVE)
				p.setGameMode(GameMode.SURVIVAL);
		} else
			p.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.prefix + "§c/g"));

		return true;
	}

	@Override
	String getPermission() {
		return "uc.g";
	}

}
