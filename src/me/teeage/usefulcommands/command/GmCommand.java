package me.teeage.usefulcommands.command;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.entity.Player;

public class GmCommand extends CMD {

	@Override
	public boolean onCommand(Player p, Command cmd, String label, String[] args) {
		if (args.length == 1) {
			if (args[0].equalsIgnoreCase("0")) {
				p.setGameMode(GameMode.SURVIVAL);
			}
			if (args[0].equalsIgnoreCase("1")) {
				p.setGameMode(GameMode.CREATIVE);
			}
			if (args[0].equalsIgnoreCase("2")) {
				p.setGameMode(GameMode.ADVENTURE);
			}
			if (args[0].equalsIgnoreCase("3")) {
				p.setGameMode(GameMode.SPECTATOR);
			}
		} else
			p.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.prefix + "§c/gm (0/1/2/3)"));
		return true;
	}

	@Override
	String getPermission() {
		return "uc.gm";
	}
}
