package me.teeage.usefulcommands.command;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.entity.Player;

public class InvseeCommand extends CMD {
	@Override
	public boolean onCommand(Player p, Command cmd, String label, String[] args) {

		if (args.length == 1) {
			Player target = Bukkit.getServer().getPlayer(args[0]);
			p.openInventory(target.getInventory());
		} else
			p.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.prefix + "§c/invsee (player)"));

		return true;
	}

	@Override
	String getPermission() {
		return "uc.invsee";
	}

}
