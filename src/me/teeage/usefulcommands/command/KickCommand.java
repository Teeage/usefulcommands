package me.teeage.usefulcommands.command;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.entity.Player;

public class KickCommand extends CMD {

	@Override
	public boolean onCommand(Player p, Command cmd, String label, String[] args) {
		if (args.length >= 2) {
			Player target = Bukkit.getPlayer(args[0]);
			if (target != null) {
				String msg = "";
				for (int i = 1; i < args.length; i++) {
					msg = msg + args[i] + " ";
				}
				target.kickPlayer(msg);
			} else
				p.sendMessage(plugin.prefix + "§cThe player " + args[0] + " is not online!");
		} else
			p.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.prefix + "§c/kick (player) [reason]"));

		return true;
	}

	@Override
	String getPermission() {
		return "uc.kick";
	}
}
