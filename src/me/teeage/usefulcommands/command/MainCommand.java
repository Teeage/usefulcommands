package me.teeage.usefulcommands.command;

import org.bukkit.command.Command;
import org.bukkit.entity.Player;

public class MainCommand extends CMD {

	@Override
	public boolean onCommand(Player p, Command cmd, String label, String[] args) {
		if (args.length == 0) {
			p.sendMessage("§c**********§7[§4UC§7]§c**********");
			p.sendMessage("§aVersion: §e" + plugin.getDescription().getVersion());
			p.sendMessage("§aAuthor: §eTeeage");
			p.sendMessage("§a/uc commands [page]");
			p.sendMessage("§c************************");
		}
		if (args.length == 1) {
			if (args[0].equalsIgnoreCase("commands")) {
				p.sendMessage("§8**********§5[§4Commands§5]§8**********");
				p.sendMessage("§b/gm <0/1/2> §5Change your gamemode");
				p.sendMessage("§b/g §5Toggle your gamemode");
				p.sendMessage("§b/heal §5Heals you");
				p.sendMessage("§b/feed §5Feeds you");
				p.sendMessage("§b/ping §5Says you your ping");
				p.sendMessage("§b/nick <name/off> §5You can give you an nickname");
				p.sendMessage("§b/fly <on/off> §5Makes fly mode on/off");
				p.sendMessage("§b/clearchat §5Clear your own chat");
				p.sendMessage("§b/kick (player) [reason]  §5Kick a player");
				p.sendMessage("§7---------§1Page(1/2)§7---------");
				p.sendMessage("§8*****************************");

			}

		}
		if (args.length == 2) {
			if (args[0].equalsIgnoreCase("commands") && args[1].equalsIgnoreCase("2")) {
				p.sendMessage("§8**********§5[§4Commands§5]§8**********");
				p.sendMessage("§b/vanish <on/off> §5Make your invisible for others");
				p.sendMessage("§b/day [world] §5Change the time to day");
				p.sendMessage("§b/night [world] §5Change the time to night");
				p.sendMessage("§b/multiworld §5Create/Delete Worlds or teleport you to worlds");
				p.sendMessage("§b/world §5You can see in wich world you are");
				p.sendMessage("§b/whois [player] §5Shows your infos about other players");
				p.sendMessage("§b/brodcast <message> §5Send a massage to all players");
				p.sendMessage("§b/fix §5If you have the ground bug, you can fix it");
				p.sendMessage("§b/invsee (Player) §5You can look in other Players Inventory");
				p.sendMessage("§7---------§1Page(2/2)§7---------");
				p.sendMessage("§8*****************************");
			}
			if (args[0].equalsIgnoreCase("commands") && args[1].equalsIgnoreCase("1")) {
				p.sendMessage("§8**********§5[§4Commands§5]§8**********");
				p.sendMessage("§b/gm <0/1/2> §5Change your gamemode");
				p.sendMessage("§b/g §5Toggle your gamemode");
				p.sendMessage("§b/heal §5Heals you");
				p.sendMessage("§b/feed §5Feeds you");
				p.sendMessage("§b/ping §5Says you your ping");
				p.sendMessage("§b/nick <name/off> §5You can give you an nickname");
				p.sendMessage("§b/fly <on/off> §5Makes fly mode on/off");
				p.sendMessage("§b/clearchat §5Clear your own chat");
				p.sendMessage("§b/kick (player) [reason]  §5Kick a player");
				p.sendMessage("§7---------§1Page(1/2)§7---------");
				p.sendMessage("§8*****************************");

			}
		}

		return true;
	}

	@Override
	String getPermission() {
		return "";
	}

}
