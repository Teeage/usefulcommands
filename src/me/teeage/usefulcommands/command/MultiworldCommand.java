package me.teeage.usefulcommands.command;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Difficulty;
import org.bukkit.World;
import org.bukkit.World.Environment;
import org.bukkit.WorldCreator;
import org.bukkit.WorldType;
import org.bukkit.command.Command;
import org.bukkit.entity.Player;

public class MultiworldCommand extends CMD {

	@Override
	public boolean onCommand(Player p, Command cmd, String label, String[] args) {
		if (args.length == 0) {
			p.sendMessage("§c*********************§7[§5HelpPage§7]§c*********************");
			p.sendMessage("§a/mw help");
			p.sendMessage("§a/mw info");
			p.sendMessage("§a/mw worlds");
			p.sendMessage("§a/mw tp (world)");
			p.sendMessage("§a/mw create (type) (world)");
			p.sendMessage("§a/mw delete (world)");
			p.sendMessage("§aTypes: §enormal§7,§eflat§7,§eamplified§7,§elarge_biomes§7,§enether§7,§eend");
			p.sendMessage("§c***************************************************");

		}
		if (args.length >= 1) {
			if (args[0].equalsIgnoreCase("help")) {
				if (args.length == 1) {
					p.sendMessage("§c*********************§7[§5HelpPage§7]§c*********************");
					p.sendMessage("§a/mw help");
					p.sendMessage("§a/mw info");
					p.sendMessage("§a/mw worlds");
					p.sendMessage("§a/mw tp (world)");
					p.sendMessage("§a/mw create (world) (type)");
					p.sendMessage("§a/mw delete (world)");
					p.sendMessage(
							"§aTypes: §eDEFAULT§7,§eFLAT§7,§eDEFAULT_1_1§7,§eLARGEBIOMES§7,§eAMPLIFIED§7,§eNETHER§7,§eEND");
					p.sendMessage("§c***************************************************");
				}
			}
			if (args[0].equalsIgnoreCase("info")) {
				if (args.length == 1) {
					World w = p.getWorld();
					String name = w.getName();
					boolean animals = w.getAllowAnimals();
					boolean monsters = w.getAllowMonsters();
					Difficulty diff = w.getDifficulty();
					Environment env = w.getEnvironment();
					boolean pvp = w.getPVP();
					p.sendMessage("§c*****§7[§5WorldInfo§7]§c*****");
					p.sendMessage("§aWorldname: §e" + name);
					p.sendMessage("§aDifficultiy: §e" + diff);
					p.sendMessage("§aEnvironment: §e" + env);
					p.sendMessage("§aAnimals: §e" + animals);
					p.sendMessage("§aMonsters §e" + monsters);
					p.sendMessage("§aPVP: §e" + pvp);
					p.sendMessage("§c*********************");
				}
			}
			if (args[0].equalsIgnoreCase("worlds")) {
				if (args.length == 1) {
					List<World> worlds = Bukkit.getServer().getWorlds();
					p.sendMessage("§c*****§7[§5Worlds§7]§c*****");
					for (int i = 0; i < worlds.size(); i++) {
						World w = worlds.get(i);
						p.sendMessage("§a" + w.getName());
					}
					p.sendMessage("§c*********************");
				}
			}
			if (args[0].equalsIgnoreCase("tp")) {
				if (args.length == 2) {
					String world = args[1];
					World w = Bukkit.getWorld(world);
					if (w != null) {
						p.teleport(w.getSpawnLocation());
						p.sendMessage(ChatColor.translateAlternateColorCodes('&',
								plugin.prefix + "§aYou are now in §5" + args[1]));

					} else {
						p.sendMessage(ChatColor.translateAlternateColorCodes('&',
								plugin.prefix + "§cThe World §5" + args[1] + " §cdoesn't exist!"));
					}
				} else {
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.prefix + "§c/mw tp (name)"));
				}
			}
			if (args[0].equalsIgnoreCase("create")) {
				if (args.length == 3) {
					if (args[2].equalsIgnoreCase("nether") || args[2].equalsIgnoreCase("end")) {

						WorldCreator w = WorldCreator.name(args[1])
								.environment(Environment.valueOf(args[2].toUpperCase()));
						p.sendMessage(ChatColor.translateAlternateColorCodes('&',
								plugin.prefix + "§4Worldcreation startet...."));
						Bukkit.createWorld(w);
						List<String> worlds = plugin.cfg.getStringList("worlds");
						worlds.add(args[2]);
						plugin.cfg.set("worlds", worlds);
						try {
							plugin.cfg.save(new File(plugin.getDataFolder() + "//worlds.yml"));
						} catch (IOException e) {
							e.printStackTrace();
						}
						p.sendMessage(ChatColor.translateAlternateColorCodes('&',
								plugin.prefix + "§4....the world " + args[1] + " was createt"));

					} else {
						WorldType wt = WorldType.getByName(args[2].toUpperCase());
						if (wt == null)
							return true;
						String worldName = args[1];
						WorldCreator w = WorldCreator.name(worldName).environment(Environment.NORMAL).type(wt)
								.generateStructures(true);
						p.sendMessage(ChatColor.translateAlternateColorCodes('&',
								plugin.prefix + "§4Worldcreation startet...."));
						Bukkit.createWorld(w);
						List<String> worlds = plugin.cfg.getStringList("worlds");
						worlds.add(args[1]);
						plugin.cfg.set("worlds", worlds);
						try {
							plugin.cfg.save(new File(plugin.getDataFolder() + "//worlds.yml"));
						} catch (IOException e) {
							e.printStackTrace();
						}
						p.sendMessage(ChatColor.translateAlternateColorCodes('&',
								plugin.prefix + "§4....the world " + args[1] + " was createt"));
					}
				} else {
					p.sendMessage(
							ChatColor.translateAlternateColorCodes('&', plugin.prefix + "§c/mw create (type) (name)"));
				}
			}
			if (args[0].equalsIgnoreCase("delete")) {
				if (args.length == 2) {
					String world = args[1];
					List<String> worlds = plugin.cfg.getStringList("worlds");
					if (!worlds.contains(world))
						return true;
					Bukkit.unloadWorld(world, false);
					worlds.remove(world);
					plugin.cfg.set("worlds", worlds);
					try {
						plugin.cfg.save(new File(plugin.getDataFolder() + "//worlds.yml"));
					} catch (IOException e) {
						e.printStackTrace();
					}
					p.sendMessage(ChatColor.translateAlternateColorCodes('&',
							plugin.prefix + "§cRemoved world §7" + args[1] + " §cfrom the database."));
				}
			}

		}

		return true;

	}

	@Override
	String getPermission() {
		return "uc.multiworld";
	}

}
