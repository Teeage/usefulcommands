package me.teeage.usefulcommands.command;

import java.util.HashMap;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

public class NickCommand extends CMD implements Listener {

	private HashMap<String, String> nick = new HashMap<>();

	@Override
	public boolean onCommand(Player p, Command cmd, String label, String[] args) {
		if (args.length == 1) {
			if (args[0].equalsIgnoreCase("off")) {
				nick.remove(p.getName());
				p.setPlayerListName(p.getName());
				p.setDisplayName(p.getName());
				p.sendMessage(ChatColor.translateAlternateColorCodes('&',
						plugin.prefix + "Your Nickname ist now §5" + p.getName()));
				return true;
			} else if (args.length == 1) {
				nick.put(p.getName(), args[0]);
				p.setDisplayName(ChatColor.translateAlternateColorCodes('&', args[0] + "§f"));
				p.setPlayerListName(ChatColor.translateAlternateColorCodes('&', args[0]));
				p.sendMessage(ChatColor.translateAlternateColorCodes('&',
						plugin.prefix + "Your Nickname ist now §5" + args[0]));

			}

		} else
			p.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.prefix + "/setnick <nickname/off>"));

		return true;
	}

	@Override
	String getPermission() {
		return "uc.nick";
	}

}
