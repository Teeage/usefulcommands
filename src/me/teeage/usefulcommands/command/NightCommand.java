package me.teeage.usefulcommands.command;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.entity.Player;

public class NightCommand extends CMD {

	@Override
	public boolean onCommand(Player p, Command cmd, String label, String[] args) {
		if (args.length == 0) {
			p.getWorld().setTime(18000);
		} else if (args.length == 1) {
			World w = Bukkit.getWorld(args[0]);
			if (w != null)
				w.setTime(18000);
		} else
			p.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.prefix + "§c/night [world]"));
		return true;
	}

	@Override
	String getPermission() {
		return "uc.night";
	}

}
