package me.teeage.usefulcommands.command;

import java.lang.reflect.InvocationTargetException;
import java.util.regex.Pattern;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.entity.Player;

public class PingCommand extends CMD {

	@Override
	public boolean onCommand(Player p, Command cmd, String label, String[] args) {
		if (args.length == 0) {
			p.sendMessage(
					ChatColor.translateAlternateColorCodes('&', plugin.prefix + "§3Your Ping is §c" + getPing(p)));

		}
		if (args.length == 1) {
			Player target = Bukkit.getServer().getPlayer(args[0]);
			p.sendMessage(ChatColor.translateAlternateColorCodes('&',
					plugin.prefix + "§7The ping from §6" + target.getName() + "§7 is " + getPing(target)));
		} else
			p.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.prefix + "§c/ping [player]"));

		return true;
	}

	public int getPing(Player p) {
		try {
			Class<?> craftPlayer = Class
					.forName("org.bukkit.craftbukkit." + getServerVersion() + ".entity.CraftPlayer");
			Class<?> entityPlayer1 = Class.forName("net.minecraft.server." + getServerVersion() + ".EntityPlayer");
			Object converted = craftPlayer.cast(p);
			Object entityPlayer2 = craftPlayer.getMethod("getHandle", new Class[0]).invoke(converted, new Object[0]);
			int ping = entityPlayer1.getField("ping").getInt(entityPlayer2);
			return ping;
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException
				| SecurityException | NoSuchFieldException | ClassNotFoundException e1) {
		}
		return 0;

	}

	private static String getServerVersion() {
		Pattern brand = Pattern.compile("(v|)[0-9][_.][0-9][_.][R0-9]*");

		String pkg = Bukkit.getServer().getClass().getPackage().getName();
		String version = pkg.substring(pkg.lastIndexOf('.') + 1);
		if (!brand.matcher(version).matches()) {
			version = "";
		}
		return version;
	}

	@Override
	String getPermission() {
		return "uc.ping";
	}

}
