package me.teeage.usefulcommands.command;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.entity.Player;

public class SpawnCommand extends CMD {

	@Override
	public boolean onCommand(Player p, Command cmd, String label, String[] args) {
		if (args.length == 0) {
			p.teleport(p.getWorld().getSpawnLocation());

		} else
			p.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.prefix + "§c/spawn"));
		return true;
	}

	@Override
	String getPermission() {
		return "uc.spawn";
	}

}
