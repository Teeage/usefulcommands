package me.teeage.usefulcommands.command;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class VanishCommand extends CMD implements Listener {

	private ArrayList<Player> vanish = new ArrayList<>();

	@Override
	public boolean onCommand(Player p, Command cmd, String label, String[] args) {
		if (args.length == 0) {
			if (vanish.contains(p)) {
				p.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.prefix + "§7Vanish is now §cOFF"));
				show(p);
			} else {
				hide(p);
				p.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.prefix + "§7Vanish is now §aON"));
			}
		}
		if (args.length == 1) {
			if (args[0].equalsIgnoreCase("on")) {
				hide(p);
				p.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.prefix + "§7Vanish is now §aON"));
			}
			if (args[0].equalsIgnoreCase("off")) {
				p.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.prefix + "§7Vanish is now §cOFF"));
				show(p);
			}
		}
		if (args.length == 2) {
			Player target = Bukkit.getServer().getPlayer(args[1]);
			if (target != null) {
				if (args[0].equalsIgnoreCase("on")) {
					hide(target);
					target.sendMessage(ChatColor.translateAlternateColorCodes('&',
							plugin.prefix + "§5" + p.getName() + "§7 make you invisible"));
				}
				if (args[0].equalsIgnoreCase("off")) {
					target.sendMessage(ChatColor.translateAlternateColorCodes('&',
							plugin.prefix + "§5" + p.getName() + "§7 make you visible"));
					show(target);
				}
			}
		} else
			p.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.prefix + "§c/vanish"));

		return true;
	}

	private void hide(Player p) {
		for (Player players : Bukkit.getOnlinePlayers()) {
			players.hidePlayer(p);
			vanish.add(p);
		}
	}

	private void show(Player p) {
		for (Player players : Bukkit.getOnlinePlayers()) {
			players.showPlayer(p);
			vanish.remove(p);
		}
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		for (Player player : vanish) {
			e.getPlayer().hidePlayer(player);
		}
	}

	@EventHandler
	public void onleave(PlayerQuitEvent e) {
		if (vanish.contains(e.getPlayer()))
			show(e.getPlayer());

		for (Player player : vanish) {
			e.getPlayer().showPlayer(player);
		}
	}

	@Override
	String getPermission() {
		return "uc.vanish";
	}
}
