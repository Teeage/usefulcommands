package me.teeage.usefulcommands.command;

import java.net.InetSocketAddress;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.entity.Player;

public class WhoIsCommand extends CMD {

	@Override
	public boolean onCommand(Player p, Command cmd, String label, String[] args) {
		if (args.length == 1) {
			Player target = Bukkit.getPlayer(args[0]);
			if (target.isOnline()) {
				InetSocketAddress IP = target.getAddress();
				String DisplayName = target.getDisplayName();
				String Name = target.getName();
				UUID UUID = target.getUniqueId();
				String World = target.getWorld().getName();
				GameMode Gamemode = target.getGameMode();
				Material item = target.getItemInHand().getType();
				Location loc = p.getLocation();

				int x = (int) loc.getX();
				int y = (int) loc.getY();
				int z = (int) loc.getZ();

				p.sendMessage("§c********§7[§5" + DisplayName + "§7]§c********");
				p.sendMessage("§aDisplayName: §5" + DisplayName);
				p.sendMessage("§aRealName: §5" + Name);
				p.sendMessage("§aUUID: §5" + UUID);
				p.sendMessage("§aIP: §5" + IP);
				p.sendMessage("§aGamemode: §5" + Gamemode);
				p.sendMessage("§aItemInHand: §5" + item);
				p.sendMessage("§aLocation: §5" + World + ", " + x + ", " + y + ", " + z);
				p.sendMessage("§c****************************");
			} else {
				@SuppressWarnings("deprecation")
				OfflinePlayer offlineTarget = Bukkit.getOfflinePlayer(args[0]);
				String name = offlineTarget.getName();
				Boolean banned = offlineTarget.isBanned();
				Boolean white = offlineTarget.isWhitelisted();
				Boolean op = offlineTarget.isOp();
				UUID uuid = offlineTarget.getUniqueId();
				p.sendMessage("§c********§7[§5" + name + "§7]§c********");
				p.sendMessage("§aUUID: §5" + uuid);
				p.sendMessage("§aisBanned: §5" + banned);
				p.sendMessage("§aisWhitelisted: §5" + white);
				p.sendMessage("§aisOP: §5" + op);
				p.sendMessage("§c****************************");
			}

		} else
			p.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.prefix + "§c/whois (player)"));

		return true;
	}

	@Override
	String getPermission() {
		return "uc.whois";
	}

}
