package me.teeage.usefulcommands.command;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.entity.Player;

public class WorldCommand extends CMD {

	@Override
	public boolean onCommand(Player p, Command cmd, String label, String[] args) {
		if (args.length == 0) {
			String world = p.getWorld().getName();
			p.sendMessage("§7The name of you current world is §5" + world);

		}
		if (args.length == 1) {
			Player target = plugin.getServer().getPlayer(args[0]);
			String world = target.getWorld().getName();
			p.sendMessage("§7The name of you current world from §5" + target.getName() + " §7is §5" + world);

		} else
			p.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.prefix + "§c/world"));

		return true;
	}

	@Override
	String getPermission() {
		return "uc.world";
	}

}
